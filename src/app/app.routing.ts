import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ApiresapacheComponent } from './components/apiresapache.component';
import { HomeComponent } from './components/home.component';
import { ErrorComponent } from './components/error.component';
import { DelitosComponent } from './components/delitos.component';
import { HomicidiosComponent } from './components/homicidios.component';
import { AgresorComponent } from './components/agresor.component';
import { HechoComponent } from './components/hecho.component';
import { FactorComponent } from './components/factor.component';
import { ResumenComponent } from './components/resumen.component';
import { AboutComponent } from './components/about.component';

import { DelincuenteComponent } from './components/delincuente.component';
import { AdddeliComponent } from './components/adddeli.component';
import { DeldeliComponent } from './components/deldeli.component';
import { ActuadeliComponent } from './components/actuadeli.component';
import { FinddeliComponent } from './components/finddeli.component';

const appRoutes: Routes = [
    {
        path: '', component: HomeComponent
    },
    {
        path: 'home', component: HomeComponent
    },
    {
        path: 'delitos', component: DelitosComponent
    },
    {
        path: 'homicidios', component: HomicidiosComponent
    },
    {
        path: 'agresor', component: AgresorComponent
    },
    {
        path: 'hecho', component: HechoComponent
    },
    {
        path: 'factor', component: FactorComponent
    },
    {
        path: 'resumen', component: ResumenComponent
    },
    {
        path: 'about', component: AboutComponent
    },
    {
        path: 'delincuente', component: DelincuenteComponent
    },
    {
        path: 'actualizar', component: ActuadeliComponent
    },
    {
        path: 'eliminar', component: DeldeliComponent
    },
    {
        path: 'adicionar', component: AdddeliComponent
    },
    {
        path: 'consultar', component: FinddeliComponent
    },
    {
        path: 'apiresapache', component: ApiresapacheComponent
    },
    {
        path: '**', component:ErrorComponent
    }

];

export const appRoutingProviders: any[] = [];
export const routing:ModuleWithProviders=RouterModule.forRoot(appRoutes);
