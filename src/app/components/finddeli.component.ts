import { Component } from '@angular/core';
import { DelincuenteService } from './../services/delincuente.service';
import { Product } from '../models/product';
//import { Headers } from '@angular/http';

@Component({
    selector: 'finddeli-list',
    templateUrl: './../views/finddeli-list.html',
    providers: [DelincuenteService]
})

export class FinddeliComponent
{
    public title: string;
    public delincuente:Product[];
    constructor(
      private _delincuenteService: DelincuenteService
    )
    {
        this.title = 'Encontrar Delincuente';
    }

    ngOnInit()
    {
        this._delincuenteService.findDelincuente().subscribe(
            result => {
                console.log(result);
                this.delincuente= result;
            },
            error => {
                console.log(<any>error);
            }
        );
    }
}
