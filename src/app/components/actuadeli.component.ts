import { Component } from '@angular/core';
import { DelincuenteService } from './../services/delincuente.service';
import { Product } from '../models/product';
//import { Headers } from '@angular/http';

@Component({
    selector: 'actuadeli-list',
    templateUrl: './../views/actuadeli-list.html',
    providers: [DelincuenteService]
})

export class ActuadeliComponent
{
    public title: string;
    public delincuente:Product[];
    constructor(
      private _delincuenteService: DelincuenteService
    )
    {
        this.title = 'Actualizar Delincuentes';
    }

    ngOnInit()
    {
        this._delincuenteService.updateDelincuente().subscribe(
            result => {
                console.log(result);
                this.delincuente= result;
            },
            error => {
                console.log(<any>error);
            }
        );
    }
}
