import { Component } from '@angular/core';
import { ResumenService } from './../services/resumen.service';

@Component({
    selector: 'resumen-list',
    templateUrl: './../views/resumen-list.html',
    providers: [ResumenService]
})

export class ResumenComponent
{
    public title: string;
    public resumen;
    constructor(private _resumenService: ResumenService)
    {
        this.title = 'Muertes según grupo de edad y tipo de muerte';
    }

    ngOnInit()
    {
        this._resumenService.getResumen().subscribe(
            result => {
                this.resumen = result;
            },
            error => {
                console.log(<any>error);
            }
        );
    }
}