import { Component } from '@angular/core';
import { FactorService } from './../services/factor.service';

@Component({
    selector: 'factor-list',
    templateUrl: './../views/factor-list.html',
    providers: [FactorService]
})

export class FactorComponent
{
    public title: string;
    public factor;
    constructor(private _factorService: FactorService)
    {
        this.title = 'Homicidios según factor de vulnerabilidad ';
    }

    ngOnInit()
    {
        this._factorService.getFactor().subscribe(
            result => {
                this.factor = result;
            },
            error => {
                console.log(<any>error);
            }
        );
    }
}