import { Component } from '@angular/core';
import { HomicidiosService } from './../services/homicidios.service';

@Component({
    selector: 'homicidios-list',
    templateUrl: './../views/homicidios-list.html',
    providers: [HomicidiosService]
})

export class HomicidiosComponent
{
    public title: string;
    public homicidios;
    constructor(private _homicidiosService: HomicidiosService)
    {
        this.title = 'Homicidios Accidente de Transito';
    }

    ngOnInit()
    {
        this._homicidiosService.getHomicidios().subscribe(
            result => {
                this.homicidios = result;
            },
            error => {
                console.log(<any>error);
            }
        );
    }
}
