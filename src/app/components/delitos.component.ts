import { Component } from '@angular/core';
import { DelitosService } from './../services/delitos.service';

@Component({
    selector: 'delitos-list',
    templateUrl: './../views/delitos-list.html',
    providers: [DelitosService]
})

export class DelitosComponent
{
    public title: string;
    public delitos;
    constructor(private _delitosService: DelitosService)
    {
        this.title = 'Delitos Segun genero';
    }

    ngOnInit()
    {
        this._delitosService.getDelitos().subscribe(
            result => {
              console.log(result);
              this.delitos = result;
            },
            error => {
                console.log(<any>error);
            }
        );
    }
}
