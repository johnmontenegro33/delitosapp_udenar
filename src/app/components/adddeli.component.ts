import { Component } from '@angular/core';
import { DelincuenteService } from './../services/delincuente.service';
import { Product } from '../models/product';
//import { Response } from '@angular/http';

//import { Headers } from '@angular/http';

@Component({
    selector: 'adddeli-list',
    templateUrl: './../views/adddeli-list.html',
    providers: [DelincuenteService]
})

export class AdddeliComponent
{
    public title: string;
    public delincuente:Product;

    constructor(
      private _delincuenteService: DelincuenteService,
      //private _route: ActivatedRoute,
      //private _route: Router
    )
    {
        this.title = 'Adicionar Delincuente';
        this.delincuente = new Product(16,"","");
    }

    ngOnInit(){
      console.log("cargando...");
    }
    onSubmit()
   {
       console.log(this.delincuente);
       this._delincuenteService.addDelincuente(this.delincuente).subscribe(
            result =>{
              console.log(result);
              alert("Delincuente registrado con exito con exito");
            },
            error => {
               alert("Error registrando al delincuente");
              console.log(<any>error);
            }
        );
    }
}
