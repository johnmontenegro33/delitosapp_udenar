import { Component } from '@angular/core';

@Component({
    selector: 'about-list',
    templateUrl: './../views/about-list.html'
})

export class AboutComponent{
    public title: string;
    constructor()
    {
        this.title = 'Acerca de';
    }

    ngOnInit()
    {

    }
}