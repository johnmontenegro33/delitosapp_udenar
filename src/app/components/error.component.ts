import { Component } from '@angular/core';

@Component({
    selector: 'error-component',
    templateUrl:'./../views/error.html'
})

export class ErrorComponent{
    public title:string;
    constructor()
    {
        this.title = 'Pagina no encontrada';
    }

    ngOnInit()
    {
      
    }
}
