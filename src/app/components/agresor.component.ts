import { Component } from '@angular/core';
import { AgresorService } from './../services/agresor.service';

@Component({
    selector: 'agresor-list',
    templateUrl: './../views/agresor-list.html',
    providers: [AgresorService]
})

export class AgresorComponent
{
    public title: string;
    public agresor;
    constructor(private _agresorService: AgresorService)
    {
        this.title = 'Homicidios según presunto agresor';
    }

    ngOnInit()
    {
        this._agresorService.getAgresor().subscribe(
            result => {
                this.agresor = result;
            },
            error => {
                console.log(<any>error);
            }
        );
    }
}
