import { Component } from '@angular/core';
import { HechoService } from './../services/hecho.service';

@Component({
    selector: 'hecho-list',
    templateUrl: './../views/hecho-list.html',
    providers: [HechoService]
})

export class HechoComponent
{
    public title: string;
    public hecho;
    constructor(private _hechoService: HechoService)
    {
        this.title = 'Homicidios según circunstancia del hecho ';
    }

    ngOnInit()
    {
        this._hechoService.getHecho().subscribe(
            result => {
                this.hecho = result;
            },
            error => {
                console.log(<any>error);
            }
        );
    }
}
