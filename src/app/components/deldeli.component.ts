import { Component } from '@angular/core';
import { DelincuenteService } from './../services/delincuente.service';
import { Product } from '../models/product';
//import { Headers } from '@angular/http';

@Component({
    selector: 'deldeli-list',
    templateUrl: './../views/deldeli-list.html',
    providers: [DelincuenteService]
})

export class DeldeliComponent
{
    public title: string;
    public delincuente:Product[];
    constructor(
      private _delincuenteService: DelincuenteService
    )
    {
        this.title = 'delincuentes';
    }

    ngOnInit()
    {
        this._delincuenteService.deleteDelincuente().subscribe(
            result => {
                console.log(result);
                this.delincuente= result;
            },
            error => {
                console.log(<any>error);
            }
        );
    }
}
