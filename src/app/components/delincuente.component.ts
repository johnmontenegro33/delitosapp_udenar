import { Component } from '@angular/core';
import { DelincuenteService } from './../services/delincuente.service';
import { Product } from '../models/product';
//import { Headers } from '@angular/http';

@Component({
    selector: 'delincuente-list',
    templateUrl: './../views/delincuente-list.html',
    providers: [DelincuenteService]
})

export class DelincuenteComponent
{
    public title: string;
    public delincuente:Product[];
    constructor(
      private _delincuenteService: DelincuenteService
    )
    {
        this.title = 'delincuentes';
    }

    ngOnInit()
    {
        this._delincuenteService.getDelincuente().subscribe(
            result => {
                console.log(result);
                this.delincuente= result;
            },
            error => {
                console.log(<any>error);
            }
        );
    }
}
