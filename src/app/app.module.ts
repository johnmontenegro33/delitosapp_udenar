import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { MDBBootstrapModule } from './typescripts/free';
import { AgmCoreModule } from '@agm/core';
import { AppComponent } from './app.component';

import { routing, appRoutingProviders} from './app.routing';

import { ApiresapacheComponent } from './components/apiresapache.component';
import { HomeComponent } from './components/home.component';
import { ErrorComponent } from './components/error.component';
import { DelitosComponent } from './components/delitos.component';
import { HomicidiosComponent } from './components/homicidios.component';
import { AgresorComponent } from './components/agresor.component';
import { HechoComponent } from './components/hecho.component';
import { FactorComponent } from './components/factor.component';
import { ResumenComponent } from './components/resumen.component';
import { AboutComponent } from './components/about.component';
//APIRES APACHE
import { DelincuenteComponent } from './components/delincuente.component';
import { AdddeliComponent } from './components/adddeli.component';
import { DeldeliComponent } from './components/deldeli.component';
import { ActuadeliComponent } from './components/actuadeli.component';
import { FinddeliComponent } from './components/finddeli.component';


import {NgbModule} from '@ng-bootstrap/ng-bootstrap';

@NgModule({
  declarations: [
    // Main Components
    AppComponent,
    // Chart Components
    ApiresapacheComponent,
    HomeComponent,
    ErrorComponent,
    DelitosComponent,
    HomicidiosComponent,
    AgresorComponent,
    HechoComponent,
    FactorComponent,
    ResumenComponent,
    AboutComponent,
    //Pipes Component
    DelincuenteComponent,
    AdddeliComponent,
    DeldeliComponent,
    ActuadeliComponent,
    FinddeliComponent

  ],
  imports: [

    // My imports
    routing,
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    HttpModule,
    MDBBootstrapModule.forRoot(),
    NgbModule.forRoot(),
    AgmCoreModule.forRoot({
      // https://developers.google.com/maps/documentation/javascript/get-api-key?hl=en#key
      apiKey: 'Your_api_key'
    })
  ],
  providers: [
      appRoutingProviders
  ],
  bootstrap: [AppComponent],
  schemas:      [ NO_ERRORS_SCHEMA ]
})
export class AppModule { }
