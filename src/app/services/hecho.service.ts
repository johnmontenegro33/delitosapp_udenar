import { Injectable } from '@angular/core';
// import { Http, Response, Headers, RequestOptions } from '@angular/http';
// import { Headers } from '@angular/http';
// import { Work } from './../models/work';
import { Http } from '@angular/http';


import 'rxjs/add/operator/map';
// import { Observable } from 'rxjs/Observable';
import { GLOBAL } from './global';

@Injectable()
export class HechoService
{
    public hecho: string;
    constructor(
        public _http:Http
    ){
        this.hecho = GLOBAL.hecho;
    }

    // To API Open Data Colombia Pinneaples ------------------------------------
    getHecho()
    {
        return this._http.get(this.hecho).map(res => res.json());
    }
}