import { Injectable } from '@angular/core';
import { Http, Headers} from '@angular/http';
import 'rxjs/add/operator/map';
//import { Observable } from 'rxjs/Observable';
import { GLOBAL } from './global';
import { Product } from '../models/product';


@Injectable()
export class DelincuenteService
{
    public delincuente: string;
    constructor(
        public _http:Http
    ){
        this.delincuente = GLOBAL.delincuente;
    }

    // To API Open Data Colombia Pinneaples ------------------------------------
    getDelincuente()
    {
      return this._http.get(this.delincuente).map(res => res.json());
    }
    deleteDelincuente()
    {
      return this._http.get(this.delincuente).map(res => res.json());
      //return this._http.delete(this.delincuente).map(res => res.json());
    }
    addDelincuente(delincuente: Product)
    {
      let headers = new Headers({'Content-Type':'application/x-www-form-urlencoded'});
      //console.log(this._http.post(this.delincuente, delincuente).map(res => res.json()));
      return this._http.post(this.delincuente, delincuente, {headers:headers}).map(res => res.json());
      //let json = JSON.stringify(delincuente);
      //let params = 'json='+json;
      //let headers = new Headers({'Content-Type':'application/x-www-form-urlencoded'});

      //return this._http.post(this.delincuente+'delincuente',params,{headers: headers})
                  //  .map(res => res.json());
      //return this._http.put(this.delincuente).map(res => res.json());
    }
    updateDelincuente()
    {
      return this._http.get(this.delincuente).map(res => res.json());
      //return this._http.put(this.delincuente).map(res => res.json());
    }
    findDelincuente()
    {
      return this._http.get(this.delincuente).map(res => res.json());
      //return this._http.get(this.delincuente).map(res => res.json());
    }
}
