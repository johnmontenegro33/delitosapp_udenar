import { Injectable } from '@angular/core';
// import { Http, Response, Headers, RequestOptions } from '@angular/http';
// import { Headers } from '@angular/http';
// import { Work } from './../models/work';
import { Http } from '@angular/http';


import 'rxjs/add/operator/map';
// import { Observable } from 'rxjs/Observable';
import { GLOBAL } from './global';

@Injectable()
export class DelitosService
{
    public delitos: string;
    constructor(
        public _http:Http
    ){
        this.delitos = GLOBAL.delitos;
    }

    // To API Open Data Colombia Pinneaples ------------------------------------
    getDelitos()
    {
      return this._http.get(this.delitos).map(res => res.json());
    }
}
