import { Injectable } from '@angular/core';
// import { Http, Response, Headers, RequestOptions } from '@angular/http';
// import { Headers } from '@angular/http';
// import { Work } from './../models/work';
import { Http } from '@angular/http';


import 'rxjs/add/operator/map';
// import { Observable } from 'rxjs/Observable';
import { GLOBAL } from './global';

@Injectable()
export class AgresorService
{
    public agresor: string;
    constructor(
        public _http:Http
    ){
        this.agresor = GLOBAL.agresor;
    }

    // To API Open Data Colombia Pinneaples ------------------------------------
    getAgresor()
    {
        return this._http.get(this.agresor).map(res => res.json());
    }
}