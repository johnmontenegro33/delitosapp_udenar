export var GLOBAL = {
    delitos: 'https://www.datos.gov.co/resource/8fcz-7ref.json',
    homicidios: 'https://www.datos.gov.co/resource/tsmv-42g7.json',
    agresor: 'https://www.datos.gov.co/resource/mths-4es9.json',
    hecho: 'https://www.datos.gov.co/resource/eu28-rabj.json',
    factor: 'https://www.datos.gov.co/resource/u242-wdd3.json',
    resumen: 'https://www.datos.gov.co/resource/me94-g3yr.json',
    delincuente:'http://localhost/SampleWS/peoples',
    header_color: 'blue'
};
