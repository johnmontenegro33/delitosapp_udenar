import { Injectable } from '@angular/core';
// import { Http, Response, Headers, RequestOptions } from '@angular/http';
// import { Headers } from '@angular/http';
// import { Work } from './../models/work';
import { Http } from '@angular/http';


import 'rxjs/add/operator/map';
// import { Observable } from 'rxjs/Observable';
import { GLOBAL } from './global';

@Injectable()
export class ResumenService
{
    public resumen: string;
    constructor(
        public _http:Http
    ){
        this.resumen = GLOBAL.resumen;
    }

    // To API Open Data Colombia Pinneaples ------------------------------------
    getResumen()
    {
        return this._http.get(this.resumen).map(res => res.json());
    }
}