import { Injectable } from '@angular/core';
// import { Http, Response, Headers, RequestOptions } from '@angular/http';
// import { Headers } from '@angular/http';
// import { Work } from './../models/work';
import { Http } from '@angular/http';


import 'rxjs/add/operator/map';
// import { Observable } from 'rxjs/Observable';
import { GLOBAL } from './global';

@Injectable()
export class HomicidiosService
{
    public homicidios: string;
    constructor(
        public _http:Http
    ){
        this.homicidios = GLOBAL.homicidios;
    }

    // To API Open Data Colombia Pinneaples ------------------------------------
    getHomicidios()
    {
        return this._http.get(this.homicidios).map(res => res.json());
    }
}
