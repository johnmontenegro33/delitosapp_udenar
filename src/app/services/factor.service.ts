import { Injectable } from '@angular/core';
// import { Http, Response, Headers, RequestOptions } from '@angular/http';
// import { Headers } from '@angular/http';
// import { Work } from './../models/work';
import { Http } from '@angular/http';


import 'rxjs/add/operator/map';
// import { Observable } from 'rxjs/Observable';
import { GLOBAL } from './global';

@Injectable()
export class FactorService
{
    public factor: string;
    constructor(
        public _http:Http
    ){
        this.factor = GLOBAL.factor;
    }

    // To API Open Data Colombia Pinneaples ------------------------------------
    getFactor()
    {
        return this._http.get(this.factor).map(res => res.json());
    }
}