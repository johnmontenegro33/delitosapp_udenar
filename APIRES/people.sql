-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 09-12-2017 a las 05:30:57
-- Versión del servidor: 10.1.21-MariaDB
-- Versión de PHP: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `dbtest`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `people`
--

CREATE TABLE `people` (
  `id` int(10) NOT NULL,
  `name` varchar(55) NOT NULL,
  `descrip` varchar(65) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `people`
--

INSERT INTO `people` (`id`, `name`, `descrip`) VALUES
(12, 'KAREN MELISSA AYALA IBARRA', 'Hurto'),
(13, 'CRISTHIAN CAMILO BASTIDAS CABRERA', 'Homicidio'),
(15, 'DANIEL ESTIBEN BETANCOURT OBANDO', 'Fabricaciontrafico'),
(19, 'MAGDALY YESENIA DELGADO VIVEROS', 'Extorsion'),
(20, 'LAURA NOHELIA ERASO OJEDA', 'Hurto'),
(21, 'CAROLINA GOMEZ SEPULVEDA', 'Homicidio'),
(27, 'YISED VANESSA MARTINEZ GUERRERO', 'Extorsion'),
(28, 'JULIAN ALEJANDRO NARANJO ERAZO', 'Hurto'),
(29, 'JESUS DAVID ORDONEZ ALVARADO', 'Homicidio'),
(35, 'LINAMARIA VALENTINA VARGAS LUNA', 'Extorsion'),
(36, 'JOSE GIOVANNI VELASCO VERDUGO', 'Hurto'),
(37, 'LUIS ALBERTO SALAZAR', 'Hurto');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `people`
--
ALTER TABLE `people`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `people`
--
ALTER TABLE `people`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=38;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
