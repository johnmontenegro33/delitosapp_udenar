Homicidios según diagnostico topográfico de la lesión y sexo

Según el diagnóstico topográfico, el politraumatismo (45,11%), el trauma craneano (22,99%) y el trauma de tórax (21,09%) explican el 89,20% de esta variable. Además de la ubicación anatómica de la lesión, estas categorías demuestran la intención letal en el homicidio, además de un alto grado de especialización en el uso de las armas utilizadas, principalmente de fuego. En el caso del trauma craneano bien se pude hablar de casos de sicariato, como practica entre los agentes del crimen sea éste organizado o no.
